const express = require("express");


// express.Router() method allows access to HTTP methods
const router = express.Router();

const taskControllers = require("../controllers/taskControllers");


// -------------------------- ADD TASK

// Create - task routes
router.post("/addTask", taskControllers.createTaskControllers);



// -------------------------- GET ALL TASK

// Get all tasks
router.get("/allTasks", taskControllers.getAllTasksController);



// -------------------------- DELETE A TASK

// Delete task
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);


// Change the status of a task to "complete"
// This route expects to receive a put request at the URL "/tasks/:id/complete"
// The whole URL is at "http://localhost:3001/tasks/:id/complete"
// We cannot use put("/tasks/:id") again because it has already been used in our route to update a task
router.put("/:id/archive", (req, res) => {
	taskControllers.changeNameToArchive(req.params.id).then(resultFromController => res.send(resultFromController));
}) 


// -------------------------- UPDATE A TASK

router.patch("/updateTask/:taskId", taskControllers.updateTaskNameController);



// -------------------------- GET SPECIFIC TASK NAME

router.get("/getSpecificName/:taskId", taskControllers.getSpecificName);


module.exports = router;












// ---------------------------------  ACTIVITY


// ---------------------------------  GET -Wildcard required


router.get("/:taskId", taskControllers.getSpecificTask);




// ---------------------------------  PUT - Update - Wildcard required


router.put("/:taskId/complete", taskControllers.updateTaskStatus);








